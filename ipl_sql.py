import psycopg2


def connect_db():
    conn = psycopg2.connect(database="ipl", user="alfa",
                            password="alfa", host="127.0.0.1", port="5432")
    return conn


def match_per_season():
    conn = connect_db()
    cur = conn.cursor()
    cur.execute('''SELECT season,count(*) from MATCHES group by season;''')
    print("\n\n\n Matches per Season    \n")
    print(cur.fetchall())
    conn.close()


match_per_season()


def win_by_all_teams():
    conn = connect_db()
    cur = conn.cursor()
    cur.execute(
        """select season, winner, count(winner) as WINS from MATCHES group by winner, season order by season;""")
    print("\n\n\n Win by All teams \n")
    print(cur.fetchall())
    conn.close()


win_by_all_teams()


def extra_runs():
    conn = connect_db()
    cur = conn.cursor()
    cur.execute("""select bowling_team, sum(extra_runs) as extra_runs from DELIVERIES join MATCHES on 
	    DELIVERIES.match_id = MATCHES.id where season=2016 group by bowling_team;""")
    print("\n\n\n Extra run conceded by teams in 2016   \n")
    print(cur.fetchall())
    conn.close()


extra_runs()

def economy_rate():
	    conn = connect_db()
	    cur = conn.cursor()
	    cur.execute("""select bowler, sum(total_runs)/(count(ball)/6) as economy_rate from DELIVERIES join matches on 
	        DELIVERIES.match_id = MATCHES.id where season=2015 group by bowler order by economy_rate limit 10;""")
	    print("\n\n\n Top Economical Bowlers    \n")
	    print(cur.fetchall())
	    conn.close()

economy_rate()